package answer;

import java.io.*;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Answer {
    
    private static final int ANSWER_LENGTH = 4;
    private static final int ANSWER_NUMBER_LIMIT = 10;
    private List<Integer> answer = null;
    
    public Answer(Path filePath) {
        try {
            List<Integer> numbers = getAnswerFromFile(filePath);
            validAnswer(numbers);
            this.setAnswer(numbers);
        } catch (Exception e) {
            System.out.println(e);
            this.setAnswer(generateRandomAnswer());
        }
    }
    
    public List<Integer> getAnswer() {
        return this.answer;
    }
    
    public void setAnswer(List<Integer> answer){
        this.answer = answer;
    }
    
    @Override
    public String toString() {
        return answer.stream().map(String::valueOf).collect(Collectors.joining(""));
    }
    
    private List<Integer> getAnswerFromFile(Path filePath) throws IOException {
        // Need to be implemented
        //return (new BufferedReader(new FileReader(new File(filePath.toString())))).readLine().chars().mapToObj(e -> Character.getNumericValue(e)).collect(Collectors.toList());
        InputStream is = ClassLoader.getSystemResourceAsStream(filePath.toString());
        return (new BufferedReader(new InputStreamReader(is))).readLine().chars().mapToObj(e -> Character.getNumericValue(e)).collect(Collectors.toList());
    }
    
    private List<Integer> generateRandomAnswer() {
        // Need to be implemented
        return Stream.iterate((new Random()).nextInt(ANSWER_NUMBER_LIMIT), n -> (new Random()).nextInt(ANSWER_NUMBER_LIMIT)).limit(ANSWER_LENGTH).collect(Collectors.toList());
    }
    
    static void validAnswer(List<Integer> answer) throws InvalidAnswerException {
        // Need to be implemented
        if(answer.size() != ANSWER_LENGTH) {
            throw new InvalidAnswerException("");
        }

        if(answer.stream().distinct().count() != ANSWER_LENGTH) {
            throw new InvalidAnswerException("");
        }
    }
}
