package game;

import answer.Answer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Game {
    
    private Answer answer;
    private Map<String, String> guessResult;
    
    public Game() {
        Path filePath = Paths.get("answer.txt");
        this.answer = new Answer(filePath);
        this.guessResult = new LinkedHashMap<>();
    }
    
    public String guess(List<Integer> numbers) {
        String result = "";
        if (numbers.equals(answer.getAnswer())) {
            result = "4A0B";
        } else {
            int countA = getASize(numbers);
            int countB = getBSize(numbers);
            result = countA + "A" + countB + "B";
        }
        String key = numbers.stream().map(String::valueOf).collect(Collectors.joining(""));
        this.guessResult.put(key, result);
        return result;
    }
    
    public boolean isOver() {
        // Need to be implemented
        GuessResult guessResult = new GuessResult(this.guessResult);
        GameResult gameResult = guessResult.getGameResult();
        return gameResult == GameResult.LOST || gameResult == GameResult.WIN;
    }
    
    public String getResult() {
        // Need to be implemented
        GuessResult guessResult = new GuessResult(this.guessResult);
        GameResult gameResult = guessResult.getGameResult();
        switch (gameResult) {
            case WIN:
                return this.answer.toString() + " " + "4A0B\n" + "Congratulations, you win!";
            case LOST:
                return guessResult.getResult() + "Unfortunately, you have no chance, the answer is " + this.answer.toString() + "!";
            default:
                return guessResult.getResult();
        }
    }
    
    private int getBSize(List<Integer> numbers) {
        // Need to be implemented
        return (int) numbers.stream().filter(t-> answer.getAnswer().contains(t) && answer.getAnswer().indexOf(t) != numbers.indexOf(t)).count();
    }
    
    private int getASize(List<Integer> numbers) {
        // Need to be implemented
        return (int) numbers.stream().filter(t-> answer.getAnswer().contains(t) && answer.getAnswer().indexOf(t) == numbers.indexOf(t)).count();
    }
}
