package game;

import java.util.Map;

public class GuessResult {
    private Map<String, String> guessRecord;
    private static final int CHANCE_LIMIT = 6;
    
    public GuessResult(Map<String, String> guessRecord) {
        this.guessRecord = guessRecord;
    }
    
    
    public String getResult() {
        // Need to be implemented
        return this.guessRecord.entrySet().stream().map(t -> t.getKey() + " " + t.getValue() + "\n").reduce("", String::concat);
    }
    
    public GameResult getGameResult() {
        // Need to be implemented
        if (guessRecord.containsValue("4A0B")) {
            return GameResult.WIN;
        }

        if (guessRecord.size() >= CHANCE_LIMIT) {
            return GameResult.LOST;
        }
        return GameResult.LOST;
    }
}
